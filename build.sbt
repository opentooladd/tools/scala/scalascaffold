name := "scalascaffold"

organization := "fr.tool-add"
version := "0.0.1"
scalaVersion := "2.12.8"

//target := baseDirectory.value / "infra/tmp/target"
//crossTarget := baseDirectory.value / "infra/tmp/crossTarget"

scalaSource in Compile := baseDirectory.value / "src/scala"


lazy val LargeTest = config("large") extend Test
lazy val MediumTest = config("medium") extend Test
lazy val SmallTest = config("small") extend Test

lazy val root = (project in file("."))
  .configs(LargeTest)
  .configs(MediumTest)
  .configs(SmallTest)
  .settings(

    inConfig(LargeTest)(Defaults.testSettings),
    scalaSource in LargeTest := baseDirectory.value / "test/large/scala",
    resourceDirectory in LargeTest := baseDirectory.value / "test/large/resources",

    inConfig(MediumTest)(Defaults.testSettings),
    scalaSource in MediumTest := baseDirectory.value / "test/medium/scala",
    resourceDirectory in MediumTest := baseDirectory.value / "test/medium/resources",

    inConfig(SmallTest)(Defaults.testSettings),
    scalaSource in SmallTest := baseDirectory.value / "test/small/scala",
    resourceDirectory in SmallTest := baseDirectory.value / "test/small/resources",

    libraryDependencies ++= Seq(
      // Logs
      "org.wvlet.airframe" %% "airframe-log" % "19.4.1",

      // Tests
      "org.scalatest" %% "scalatest" % "3.0.5" % "large,medium,small",
//      "org.mockito" % "mockito-core" % "2.23.0" % "large,medium,small",
    )
  )
