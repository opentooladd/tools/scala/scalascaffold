
## Global commands
#######################

# start docker containers; To run before any other command.
start:
	docker-compose run scala sbt compile
#	COMPOSE_HTTP_TIMEOUT=300 docker-compose up -d

# Stop containers
#stop:
#	docker-compose stop

down:
	docker-compose down --rmi all -v --remove-orphans

clean:
	docker-compose run scala sbt clean

docker-registry-deploy:
	docker login registry.gitlab.com
	docker build -t registry.gitlab.com/opentooladd/scalascaffold:latest ./infra/scala
	docker push registry.gitlab.com/opentooladd/scalascaffold:latest


## Containers access commands
##############################

#shell-scala:
#	docker exec -it scala bash


## Tests commands
##################

test-small:
	docker-compose run scala sbt small:test
#	docker-compose exec -it scala sh -c 'sbt small:test'

test-medium:
	docker-compose run scala sbt medium:test

test-large:
	docker-compose run scala sbt large:test

test-full: test-small
test-full: test-medium
test-full: test-large
