# ScalaScaffold

[![pipeline status](https://gitlab.com/opentooladd/scalascaffold/badges/master/pipeline.svg)](https://gitlab.com/opentooladd/scalascaffold/commits/master)

This project define a default scala project structure.

## Tests

This project presents a default testing setup.

We use testing model based on [Test Size](https://testing.googleblog.com/2010/12/test-sizes.html)

In order to run,
* all tests : ```make test-full```
* small tests : ```make test-small```
* medium tests : ```make test-medium```
* large tests : ```make test-large```


> remember that ```make start``` setup docker environment (not mandatory if no service required)


## Advices

We hardly recommend to develop according
[TDD](https://en.wikipedia.org/wiki/Test-driven_development) strategy.

If you want to publish your project, take a look at [sbt documentation](https://www.scala-sbt.org/1.0/docs/Publishing.html)
> For example, run ```sbt publishLocal``` in order to publish this lib locally

## Todo

* Documentation : add useful links list
* improve CI tests duration
* Manage docker image versioning
